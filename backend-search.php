<?php
require "config.php";
session_start();
$id = $_SESSION['id'];

if (isset($_REQUEST["term"])) {
    // search from the whole table studenti
    // $sql = "SELECT * FROM studenti WHERE firstName LIKE :term OR lastName LIKE :term";
    
    // search the table studenti only created by the logged in user
    $sql = "SELECT * FROM studenti WHERE firstName LIKE :term AND user_id = '" . $id . "' OR lastName LIKE :term AND user_id = '" . $id . "' ";

    $stmt = $pdo->prepare($sql);
    $term = $_REQUEST["term"] . '%';
    // bind na parametri
    $stmt->bindParam(":term", $term);
    // izvrshuvanje na stmt
    $stmt->execute();
    if ($stmt->rowCount() > 0) {
        while ($row = $stmt->fetch()) {
            echo "<a href=\"view.php?id=" . $row['s_id'] . "\" >" . $row['firstName'] . " " . $row['lastName'] . "</a>" . "";
        }
    } else {
        echo "<a class='buttons confirm'>No student with that name</a>";
    }
}
unset($stmt);
unset($pdo);
