<?php
$title = 'View Record - Student Grader';
$page = 'students';
require "navbar.php";

if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}

if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
    require_once "config.php";

    // prepare SQL
    // $sql = "SELECT * FROM studenti JOIN courses ON studenti.course_id = courses.id";
    // $sql = "SELECT * FROM studenti WHERE id = :id";
    $sql = "SELECT * FROM studenti INNER JOIN courses ON studenti.course_id = courses.c_id WHERE s_id = :id";

    if ($stmt = $pdo->prepare($sql)) {

        $stmt->bindParam(":id", $param_id);
        $param_id = trim($_GET["id"]);

        if ($stmt->execute()) {
            if ($stmt->rowCount() == 1) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $name = $row["firstName"].' '.$row['lastName'];
                $course = $row["course_name"];
                $grade = $row["grade"];
            } else {
                header("location: error.php");
                exit();
            }
        } else {
            echo "Smth went wrong";
        }
    }
    unset($stmt);
    unset($pdo);
} else {
    // url-to
    header("location:error.php");
    exit();
}

?>

<body>
    <div class="form-container">
        <div class="header-label">
            <h2>View Record</h2>
        </div>
        <hr>
        <p>Record from database for the student <?= $name ?>.</p>
        <div class="form-update-wrapper">

            <form action="update-form.php" method="POST" class="form-update">
                <label class="view-bold" for="name">Student Name</label>
                <p><?php echo $name; ?></p>
                <hr>
                <label class="view-bold" for="course">Course Name</label>
                <p><?php echo $course; ?></p>
                <hr>
                <label class="view-bold" for="grade">Grade</label>
                <p class="mg-btm"><?php echo $grade; ?></p>
                <div class="button-wrapper">
                    <a href="index.php" class="buttons back">Back to students</a>
                </div>
            </form>


        </div>

    </div>

    <?php require "footer.php"; ?>
</body>

</html>