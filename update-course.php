<?php
$title = 'Update Course - Student Grader';
$page = 'courses';
require "navbar.php";


if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}

// if there is zero courses then redirect to create course.php
if (($_SESSION['noCoursesCreated']) == true && isset($_SESSION['noCoursesCreated'])) {
    header('location: create-course.php');
    exit;
}
// include config
require_once "config.php";

// definirame promenlivite od formata i gi inicijalizira so prazna vrednost
$course_name = $course_desc = "";
$course_name_err = $course_desc_err = "";

/*

ako imame POST request isset POST !empty {

    procesiraj go i updajtiraj vo baza
} else {

ako postoi GET["id"], go zemamo toj id od url-to so get i pravime select statement

SELECT * FROM studenti WHERE id = :id;

name
course
grade

}
*/

// if 1 start
if (isset($_POST["id"]) && !empty($_POST["id"])) {
    //ovde kje se izvrshuva update

    //Validacija na name

    $id = $_POST["id"];
    $input_course_name = trim($_POST["course_name"]);
    if (empty($input_course_name)) {
        $course_name_err = "Please enter course name";
    } else {
        $course_name = $input_course_name;
    }

    //Validacija na course
    $input_course_desc = trim($_POST["course_desc"]);
    if (empty(trim($input_course_desc))) {
        $course_desc_err = "Please enter course desc.";
    } else {
        $course_desc = trim($input_course_desc);
    }

    if (empty($course_name_err) && empty($course_desc_err)) {
        $sql = "UPDATE courses SET course_name = :course_name, course_desc = :course_desc, user_id = :user_id WHERE c_id = :id";

        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":course_name", $param_course_name);
            $stmt->bindParam(":course_desc", $param_course_desc);
            $stmt->bindParam(":id", $param_id);
            $stmt->bindParam(":user_id", $param_user_id);

            //set na parametrite
            $param_course_name = $course_name;
            $param_course_desc = $course_desc;
            $param_id = $id;
            $param_user_id = $_SESSION['id'];

            if ($stmt->execute()) {
                header("location: manage-courses.php");
                exit();
            } else {
                echo "Smth went wrong";
            }
            unset($stmt);
        }
        unset($stmt);
    }
    // unset($pdo);
} else {

    if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
        //ovde go zemame URL parametarot
        $id = trim($_GET["id"]);

        // SQL stmt
        $sql = "SELECT * FROM courses WHERE c_id = :id";

        //Prepare
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":id", $param_id);
            //Set
            $param_id = $id;

            //Execute
            if ($stmt->execute()) {
                if ($stmt->rowCount() == 1) {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);

                    $course_name = $row["course_name"];
                    $course_desc = $row["course_desc"];
                } else {
                    // nema validen id paramatar

                    header("location: error.php");
                    exit();
                }
            } else {
                echo "Smth went wrong";
            }
        }
        unset($stmt);
        // unset($pdo);
    } else {
        header("location: error.php");
        exit();
    }
}

// if 1 finish

?>

<body>
    <div class="form-container">
        <div class="header-label">
            <h2>Update Course</h2>
        </div>
        <hr>
        <p>Please edit the input values and submit to update the record.</p>
        <div class="form-update-wrapper">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="form-update">

                <div class="form-group <?php echo (!empty($course_name_err)) ? 'has-error' : ''; ?>">
                    <label for="course_name">Course Name</label>
                    <input type="text" name="course_name" id="name" placeholder="Insert Course Name" value="<?php echo $course_name; ?>">
                    <span class="help-block"><?php echo $course_name_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($course_desc_err)) ? 'has-error' : ''; ?>">
                    <label for="course_desc">Course Description</label>
                    <input type="text" name="course_desc" id="name" placeholder="Insert Course Description" class="mg-btm" value="<?php echo $course_desc; ?>">
                    <span class="help-block"><?php echo $course_desc_err; ?></span>
                </div>

                <input type="hidden" name="id" value="<?php echo $id; ?>">

                <div class="button-wrapper">
                    <input type="submit" id="yes-btn" class="buttons confirm" value="Submit">
                    <!-- <a href="#" id="yes-btn" class="buttons confirm">Submit</a> -->
                    <a href="manage-courses.php" class="buttons cancel">Cancel<i class="fas fa-undo"></i></a>
                </div>

            </form>
        </div>

    </div>
    <?php require "footer.php"; ?>
</body>

</html>