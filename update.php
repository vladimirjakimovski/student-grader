<?php
$title = 'Update Record - Student Grader';
$page = 'students';
require "navbar.php";


if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}
// include config
require_once "config.php";

// definirame promenlivite od formata i gi inicijalizira so prazna vrednost
$firstName = $lastName = $course_id = $grade = "";
$firstName_err = $lastName_err = $course_id_err = $grade_err = "";

/*

ako imame POST request isset POST !empty {

    procesiraj go i updajtiraj vo baza
} else {

ako postoi GET["id"], go zemamo toj id od url-to so get i pravime select statement

SELECT * FROM studenti WHERE id = :id;

name
course
grade

}
*/

// if 1 start
if (isset($_POST["id"]) && !empty($_POST["id"])) {
    //ovde kje se izvrshuva update
    $id = $_POST["id"];

    //Validacija na name
    $input_firstName = trim($_POST["firstName"]);
    if (empty($input_firstName)) {
        $firstName_err = "Please enter first name";
    } elseif (!filter_var($input_firstName, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z ]*$/")))) {
        $firstName_err = "Please enter valid first name.";
    } else {
        $firstName = $input_firstName;
    }

    $input_lastName = trim($_POST["lastName"]);
    if (empty($input_lastName)) {
        $lastName_err = "Please enter last name";
    } elseif (!filter_var($input_lastName, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z ]*$/")))) {
        $lastNamee_err = "Please enter valid last name.";
    } else {
        $lastName = $input_lastName;
    }
    

    //Validacija na course

    if (empty(trim($_POST["course_id"]))) {
        $course_id_err = "Please enter course.";
    } else {
        $course_id = trim($_POST["course_id"]);
    }

    //Validacija na grade

    if (empty(trim($_POST["grade"]))) {
        $grade_err = "Please enter grade.";
    } elseif (!ctype_digit($_POST["grade"]) ) {
        $grade_err = "Please enter a numeric grade.";
    } elseif (intval($_POST["grade"]) > 6 ) {
        $grade_err = "Please enter a numeric grade from 1 to 5.";
    } else {
        $grade = trim($_POST["grade"]);
    }
    
    if(empty($firstName_err) && empty($lastName_err) && empty($course_id_err) && empty($grade_err)) {
        $sql = "UPDATE studenti SET firstName = :firstName, lastName = :lastName, course_id = :course_id, grade = :grade, user_id = :user_id WHERE s_id = :id";

        if($stmt = $pdo -> prepare($sql)) {
            $stmt -> bindParam(":firstName", $param_firstName);
            $stmt -> bindParam(":lastName", $param_lastName);
            $stmt -> bindParam(":course_id", $param_course_id);
            $stmt -> bindParam(":grade", $param_grade);
            $stmt -> bindParam(":id", $param_id);
            $stmt -> bindParam(":user_id", $param_user_id);


            //set na parametrite
            $param_firstName = $firstName;
            $param_lastName = $lastName;
            $param_course_id = $course_id;
            $param_grade = $grade;
            $param_id = $id;
            $param_user_id = $_SESSION['id'];

            if($stmt->execute()) {
                print_r( $_POST );

                header("location: index.php");
                
                exit();
            } else {
                echo "Smth went wrong";
            }
            unset($stmt);
        }
        unset($stmt);
        
    }
    unset($pdo);
} else {

    if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
        //ovde go zemame URL parametarot
        $id = trim($_GET["id"]);

        // SQL stmt
        $sql = "SELECT * FROM studenti WHERE s_id = :id";

        //Prepare
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":id", $param_id);
            //Set
            $param_id = $id;

            //Execute
            if ($stmt->execute()) {
                if ($stmt->rowCount() == 1) {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);

                    $firstName = $row["firstName"];
                    $lastName = $row["lastName"];
                    $course_id = $row["course_id"];
                    $grade = $row["grade"];
                } else {
                    // nema validen id paramatar

                    header("location: error.php");
                    exit();
                }
            } else {
                echo "SMth went wrong";
            }
        }
        unset($stmt);
        // unset($pdo);
    } else {
        header("location: error.php");
        exit();
    }
}

// if 1 finish

?>

<body>
    <div class="form-container mg-25">
        <div class="header-label">
            <h2>Update Record</h2>
        </div>
        <hr>
        <p>Please edit the input values and submit to update the record.</p>
        <div class="form-update-wrapper">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="form-update">

                <div class="form-group <?php echo (!empty($firstName_err)) ? 'has-error' : ''; ?>">
                    <label for="name">Student First Name</label>
                    <input type="text" name="firstName" id="name" placeholder="Insert student first name" value="<?php echo $firstName; ?>">
                    <span class="help-block"><?php echo $firstName_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                    <label for="name">Student Last Name</label>
                    <input type="text" name="lastName" id="name" placeholder="Insert student last name" value="<?php echo $lastName; ?>">
                    <span class="help-block"><?php echo $lastName_err; ?></span>
                </div>


                <div class="form-group <?php echo (!empty($course_err)) ? 'has-error' : ''; ?>">
                    <label for="course">Course</label>
                    <select required id="course" name="course_id">
                        <option disabled selected>Insert course name</option>
                        <?php
                        $sql = "SELECT * FROM courses";
                        if ($result = $pdo->query($sql)) {
                            if ($result->rowCount() > 0) {
                                while ($row = $result->fetch()) { ?>
                                    
                                    <option value="<?= $row['c_id']; ?>" <?php if($row['c_id'] == $_GET['param_course_id']) echo 'selected="selected"' ?> > <?= $row['course_name']; ?></option>;
                                <?php } ?>
                    </select>
            <?php
                            }
                            unset($result);
                        } else {
                            echo "We dont have record in the DB";
                        }
                        unset($pdo);
            ?>

            <!-- <input type="text" name="course" id="course" placeholder="Insert course name" value="<?php echo $course; ?>"> -->
            <span class="help-block"><?php echo $course_id_err; ?></span>
                </div>
<!-- FINISH COURSE -->
                <div class="form-group <?php echo (!empty($grade_err)) ? 'has-error' : ''; ?>">
                    <label for="grade">Grade</label>

                    <select required id="grade" name="grade">
                        <option hidden disabled selected>Update grade for the course</option>
                        <?php  
                            for($i = 1; $i<=5; $i++) {?>
                                <option value="<?= $i ?>"<?php if($grade == $i) echo 'selected="selected"' ?> > <?= $i ?></option>;
                        <?php } ?>
                    </select>
                    <!-- <input type="text" name="grade" id="grade" placeholder="Insert grade for the course" value="php echo grade...."> -->
                    <span class="help-block"><?php echo $grade_err; ?></span>
                </div>
                
                <input type="hidden" name="id" value="<?= $id; ?>">
                <div class="button-wrapper">
                    <input type="submit" id="yes-btn" class="buttons confirm" value="Submit">
                    <!-- <a href="#" id="yes-btn" class="buttons confirm">Submit</a> -->
                    <a href="index.php" class="buttons cancel">Cancel<i class="fas fa-undo"></i></a>
                </div>
            </form>


        </div>

    </div>
    <?php require "footer.php"; ?>
</body>

</html>