<?php
$title = 'Instructor Details - Student Grader';
$page = 'edit';
require "navbar.php";
require "config.php";

if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}

// if there is zero courses then redirect to create course.php
if (($_SESSION['noCoursesCreated']) == true) {
    header('location: create-course.php');
    exit;
}
?>

<body>
    <!-- alert pop up for added student -->
    <div id="blackout"></div>
    <div id="modal">
        <button id="closeButton">close</button>
        <div class="message">
            <h5>Alert</h5>
            <i class="fas fa-info-circle fa-4x"></i>
            <h3>The record has been succesfully inserted into the database!</h3>
        </div>
    </div>

    <div class="table-container">
        <div class="student-details">
            <h2>Instructor Details</h2>
            <a href="reset-password.php" class="buttons warning">Reset Password</a>
        </div>
        <hr>

        <table id="grader-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Instructor Name</th>
                    <th>Courses</th>
                    <th>Email</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <?php
            // $sql = "SELECT * FROM studenti";
            $sql = "SELECT * FROM users LEFT JOIN courses ON users.id = courses.user_id WHERE users.id = :id";
            if ($stmt = $pdo->prepare($sql)) {
                $stmt->bindParam(":id", $param_id);
                $param_id = $_SESSION['id'];

                    if($stmt->execute()){
                        if($stmt->rowCount()>=0) {
                            $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
                            $id = $param_id;
                            $firstName = $row[0]['firstName'];
                            $lastName = $row[0]['lastName'];
                            $email = $row[0]['email'];
                            
                        } else {
                            header("location: error.php");
                            exit();
                        }
                    } else {
                        echo "Something went wrong";
                    }
                    ?>
                        <tr>
                            <td><?= $i=1; ?></td>
                            <td><?= $firstName ." ". $lastName; ?></td>
                            
                            <td><?php

                            foreach ($row as $courseName) {
                                $total = sizeof($row);

                                if($i <= $total)
                                echo $courseName["course_name"];
                                
                                if($i < $total) 
                                    echo ", ";
                                //  if($i>=$total){
                                //     echo ".";
                                // }
                                $i++;
                            }
                            ?></td>

                            <td><?= $email; ?></td>
                            <td class="text-center">
                                <a href="update-instructor.php?id=<?= $id; ?>"><i class="fas fa-user-edit"></i></a>
                            </td>
                        </tr>
                    

        </table>
    </div>
<?php
                
                unset($result);
            } else {
                echo "We dont have record in the DB";
            }

            unset($pdo);
?>

<?php require "footer.php";?>




</body>

</html>