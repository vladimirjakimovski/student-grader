<?php
$title = 'Error - Student Grader';
require "navbar.php";


?>

<body>
    <div class="form-container">
        <div class="header-label">
            <h2>Error 404</h2>
        </div>
        <hr>

        <div class="form-update-wrapper">

            <form action="update-form.php" method="POST" class="form-update">
               
                <p>Sorry this is a invalid request</p>
                <div class="button-wrapper">
                    <a href="index.php" class="buttons back">Back</a>
                </div>
            </form>



        </div>

    </div>

    <?php require_once "partials/footer.php"; ?>
</body>

</html>