<?php
session_start();
require "head.php";
?>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script>
    var $j = jQuery.noConflict();
    $j(document).ready(function() {
        $('.search-box input[type="search"]').on('keyup', function() {
            // Take the input field value when that input value is changed
            var inputVal = $(this).val();
            var resultDropdown = $(this).siblings('.result');
            if (inputVal.length) {
                $j.get('backend-search.php', {
                    term: inputVal
                }).done(function(data) {
                    // show file result
                    resultDropdown.html(data);
                });
            } else {
                resultDropdown.empty();
            }
        });
        $(document).on('click', '.result p', function() {
            $(this).parents('.search-box').find('input[type="search"]').val($(this).text());
            $(this).parent('.result').empty();
        });
    });
</script>
<div class="header-container">
    <header>
        <a id="logo" href="index.php">
            <h1>student<b>GRADER</b></h1>
        </a>
        <?php if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) : ?>
            <div class="account-search">
            <div class="search-box">
                    <input type="search" id="search" name="term" autocomplete="off" placeholder="Search Students..." />
                    <div class="result">

                    </div>
                </div>
                <div class="buttons-container dropdown">
                    <button class="dropbtn">My Account<i class="fas fa-chevron-circle-down"></i></button>
                    <div class="dropdown-content">
                        <!-- premesti ga u instructor details edit profile -->
                        <!-- <a href="reset-password.php" class="buttons warning">Reset Password</a> -->
                        <a <?= ($page == 'edit') ? "class='activeA'" : ""; ?> href="edit-profile.php">Edit Profile</a>
                        <a <?= ($page == 'courses') ? "class='activeA'" : ""; ?> href="manage-courses.php">Manage Courses</a>
                        <a <?= ($page == 'students') ? "class='activeA'" : ""; ?> href="index.php">Manage Students</a>
                        <a href="logout.php" class="logout">Logout <?php echo $_SESSION['username']; ?> &nbsp;<i class="fas fa-sign-out-alt"></i></a>
                    </div>
                </div>
                
            </div>
        <?php endif; ?>
    </header>
</div>