<?php
$title = 'Manage Courses - Student Grader';
$page = 'courses';
require "navbar.php";
require "config.php";

if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}

// if there is zero courses then redirect to create course.php
if (($_SESSION['noCoursesCreated']) == true) {
    header('location: create-course.php');
    exit;
}

?>

<body>
        <!-- alert pop up for added course record -->
        <div id="blackout"></div>
        <div id="modal">
        <button id="closeButton">close</button>
            <div class="message">
                <h5>Alert</h5>
                <i class="fas fa-info-circle fa-4x"></i>
                <h3>The record has been succesfully inserted into the database!</h3>
            </div>
        </div>
    <div class="table-container">
        <div class="student-details">
            <h2>Course Details</h2>
            <a href="create-course.php" class="buttons confirm">Add New Course</a>
        </div>
        <hr>

        <table id="grader-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Course Name</th>
                    <th>Instructor Name</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <?php
            $id = trim($_SESSION['id']);

            print_r($_SESSION);

            #INNER JOIN courses ON studenti.course_id = courses.c_id WHERE studenti.user_id = :id
            $sql = "SELECT * FROM courses INNER JOIN users ON courses.user_id = users.id WHERE courses.user_id = :id";

            $result = $pdo->prepare($sql);

            $result->bindParam(':id', $param_id);

            $param_id = $id;

            if ($result->execute()) {
                if ($result->rowCount() > 0) {
                    $i=1;
                    while ($row = $result->fetch()) { ?>
                        <tr>
                            <td><?= $i; ?></td>
                            <td><?= $row['course_name']; ?></td>
                            <td><?= $row['firstName']; ?> <?= $row['lastName']; ?></td>
                            <td class="text-center">
                                <a href="view-course.php?id=<?= $row['c_id'] ?>"><i class="far fa-eye"></i></a>
                                <a href="update-course.php?id=<?= $row['c_id'] ?>&param_course_id=<?= $row['c_id']; ?>"><i class="fas fa-user-edit"></i></a>
                                <a href="delete-course.php?id=<?= $row['c_id'] ?>"><i class="fas fa-user-minus"></i></a>
                            </td>
                        </tr>
                    <?php $i=$i+1;} ?>
                    
        </table>
    </div>
<?php
                } else {
                    echo "<h1>The table is empty :/ Add courses</h1>";
                }
                unset($result);
            } else {
                echo "We dont have record in the DB";
            }

            unset($pdo);
?>

<?php require "footer.php"; 
   
   $recordAdded = false;

   if (isset($_SESSION['added']) && ( $_SESSION['added'] == true)) {
       $recordAdded = true;
   }

   if ($recordAdded) {
       echo '
   <script type="text/javascript">
   function hideMsg()
   {
       document.getElementById("modal").style.display = "none";
       document.getElementById("blackout").style.display = "none";
   }
   document.getElementById("modal").style.display = "block";
   document.getElementById("blackout").style.display = "block";
   
   window.setTimeout("hideMsg()", 3000);
   
   </script>';
   }
   unset($_SESSION['added']);
   $recordAdded = false;
?>




</body>

</html>