<?php
$title = 'Create Course - Student Grader';
$page = 'courses';
require "navbar.php";
require_once "config.php";

if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}

// if there is zero courses then redirect to create course.php
// if (($_SESSION['noCoursesCreated']) == true ) {
//     header('location: create-course.php');
//     exit;
// }


$course_name = $course_desc = "";
$course_name_err = $course_desc_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (empty(trim($_POST["course_name"]))) {
        $course_name_err = "Please enter course name.";
    } else {
        $course_name = trim($_POST["course_name"]);
    }

    if (empty(trim($_POST["course_desc"]))) {
        $course_desc_err = "Please enter course description";
    } else {
        $course_desc = trim($_POST["course_desc"]);
    }



    if (empty($course_name_err) && empty($course_desc_err)) {
        $sql = "INSERT INTO courses (course_name, course_desc, user_id) VALUES (:course_name, :course_desc, :user_id)";

        if ($stmt = $pdo->prepare($sql)) {

            $stmt->bindParam(":course_name", $param_course_name);
            $stmt->bindParam(":course_desc", $param_course_desc);
            $stmt->bindParam(":user_id", $param_user_id);

            $param_course_name = $course_name;
            $param_course_desc = $course_desc;
            $param_user_id = $_SESSION['id'];
        }

        if ($stmt->execute()) {
            /* Kreirajte poraka za uspeshno vnesen record i istata isprintajte ja vo index.php
            vo box koj kje stoi 7 sekundi i potoa kje ischezna */
            $_SESSION['added'] = true;
            $_SESSION['noCoursesCreated'] = false;
            
            header("location: manage-courses.php");
            exit();
        } else {
            echo "Something went wrong";
        }
        unset($stmt);
    } // end 2 if
    unset($pdo);
} // end 1 if
?>

<body>
    <div class="form-container">
        <div class="header-label">
            <h2>Create Record</h2>
        </div>
        <hr>
        <p>Please fill this form and submit to add course record to the database.</p>
        <div class="form-update-wrapper">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="form-update">

                <div class="form-group <?php echo (!empty($course_name_err)) ? 'has-error' : ''; ?>">
                    <label for="course_name">Course Name</label>
                    <input type="text" name="course_name" placeholder="Insert course name" value="<?php echo $course_name; ?>">
                    <span class="help-block"><?php echo $course_name_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($course_desc_err)) ? 'has-error' : ''; ?>">
                    <label for="course_desc">Course Description</label>
                    <input type="text" name="course_desc" placeholder="Insert course description" class="mg-btm" value="<?php echo $course_desc; ?>">
                    <span class="help-block"><?php echo $course_desc_err; ?></span>
                </div>

                <div class="button-wrapper">
                    <input type="submit" id="yes-btn" class="buttons confirm" value="Submit">
                    <!-- <a href="#" id="yes-btn" class="buttons confirm">Submit</a> -->
                    <a href="manage-courses.php" class="buttons cancel">Cancel<i class="fas fa-undo"></i></a>
                </div>
            </form>



        </div>

    </div>
    <?php require "footer.php"; ?>
</body>

</html>