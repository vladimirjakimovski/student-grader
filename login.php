<?php
require_once "config.php";

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    header('location: create-course.php');
    exit;
}


$username = $password = "";
$username_err = $password_err = "";

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    // dali username e prazen
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter your username.";
    } else {
        $username = trim($_POST["username"]);
    }
    // dali password e prazen
    if (empty(trim($_POST["password"]))) {
        $password_err = "Please enter your password.";
    } else {
        $password = trim($_POST["password"]);
    }

    if (empty($username_err) && empty($password_err)) {
        $sql = "SELECT id, username, password FROM users WHERE username = :username";
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":username", $param_username);

            $param_username = trim($_POST["username"]);

            if ($stmt->execute()) {
                // da proverime dali usernamot postoi

                if ($stmt->rowCount() == 1) {
                    if ($row = $stmt->fetch()) {
                        $id = $row['id'];
                        $username = $row['username'];
                        $hashed_password = $row['password'];
                        if (password_verify($password, $hashed_password)) {
                            session_start();

                            $_SESSION['loggedin'] = true;
                            $_SESSION['id'] = $id;
                            $_SESSION['username'] = $username;
                            
                            $_SESSION['noCoursesCreated'] = true;

                            header("location: create-course.php");
                        } else {
                            // greshka za password

                            $password_err = "The password you entered is not correct";
                        }
                    }
                } else {
                    $username_err = "No user with that username";
                }
            } else  echo "smtg is wrong";
        }
        unset($stmt);
    }
    unset($pdo);
}
// end if POST
$title = "Log In";
require "navbar.php";

?>



<body>
    <div class="form-container login">


            <div class="login">
                <h2>Login</h2>
                <p>Please fill in your credentials to login.</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                        <span class="help-block"><?php echo $username_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control">
                        <span class="help-block"><?php echo $password_err; ?></span>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Login">
                    </div>
                    <p>Don't have an account? <a href="register.php">Sign up now</a>.</p>
                </form>
            </div>

    </div>
    <?php require "footer.php"; ?>
</body>

</html>