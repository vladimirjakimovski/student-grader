<?php
$title = 'Create Record - Student Grader';
$page = 'students';
require "navbar.php";
require_once "config.php";

ini_set('display_errors', 1);
error_reporting(-1);

// if there is no one logged in redirect it login page
if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}

// if there is zero courses then redirect to create course.php
if (($_SESSION['noCoursesCreated']) == true && isset($_SESSION['noCoursesCreated'])) {
    header('location: create-course.php');
    exit;
}


$firstName = $lastName = $course_id = $grade = "";
$firstName_err = $lastName_err = $course_id_err = $grade_err = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $input_firstName = trim($_POST["firstName"]);
    if (empty($input_firstName)) {
        $firstName_err = "Please enter first name";
    } elseif (!filter_var($input_firstName, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z ]*$/")))) {
        $firstName_err = "Please enter valid first name.";
    } else {
        $firstName = $input_firstName;
    }

    $input_lastName = trim($_POST["lastName"]);
    if (empty($input_lastName)) {
        $lastName_err = "Please enter last name";
    } elseif (!filter_var($input_lastName, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z ]*$/")))) {
        $lastNamee_err = "Please enter valid last name.";
    } else {
        $lastName = $input_lastName;
    }


    if (empty(trim($_POST["course_id"]))) {
        $course_id_err = "Please enter course.";
    } else {
        $course_id = trim($_POST["course_id"]);
    }

    if (empty(trim($_POST["grade"]))) {
        $grade_err = "Please enter grade.";
    } elseif (!ctype_digit($_POST["grade"])) {
        $grade_err = "Please enter a numeric grade.";
    } else {
        $grade = trim($_POST["grade"]);
    }

    if (empty($firtName_err) && empty($lastName_err) &&  empty($course_id_err) && empty($grade_err)) {
        $sql = "INSERT INTO studenti (firstName, lastName, course_id, grade, user_id) VALUES (:firstName, :lastName, :course_id, :grade, :user_id)";
        #INSERT INTO users (username) VALUES (:username)";

        if ($stmt = $pdo->prepare($sql)) {

            $stmt->bindParam(":firstName", $param_firstName);
            $stmt->bindParam(":lastName", $param_lastName);
            $stmt->bindParam(":course_id", $param_course_id);
            $stmt->bindParam(":grade", $param_grade);
            $stmt->bindParam(":user_id", $param_user_id);

            $param_firstName = $firstName;
            $param_lastName = $lastName;
            $param_course_id = $course_id;
            $param_grade = $grade;
            $param_user_id = $_SESSION['id'];
        }

        if ($stmt->execute()) {
            /* Kreirajte poraka za uspeshno vnesen record i istata isprintajte ja vo index.php
            vo box koj kje stoi 7 sekundi i potoa kje ischezna */
            $_SESSION['added'] = true;
            header("location: index.php");

            exit();
        } else {
            echo "Something went wrong";
        }
        unset($stmt);
    } // end 2 if
    unset($pdo);
} // end 1 if
?>

<body>
    <div class="form-container">
        <div class="header-label">
            <h2>Create Record</h2>
        </div>
        <hr>
        <p>Please fill this form and submit to add student record to the database.</p>
        <div class="form-update-wrapper">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="form-update">

                <div class="form-group <?php echo (!empty($firstName_err)) ? 'has-error' : ''; ?>">
                    <label for="name">Student First Name</label>
                    <input type="text" name="firstName" id="name" placeholder="Insert student first name" value="<?php echo $firstName; ?>">
                    <span class="help-block"><?php echo $firstName_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($lastName_err)) ? 'has-error' : ''; ?>">
                    <label for="name">Student Last Name</label>
                    <input type="text" name="lastName" placeholder="Insert student last name" value="<?php echo $lastName; ?>">
                    <span class="help-block"><?php echo $lastName_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($course_err)) ? 'has-error' : ''; ?>">
                    <label for="course">Course Name</label>
                    <select required id="course" name="course_id">
                        <option hidden disabled selected>Insert course name</option>
                        <?php
                        $id = trim($_SESSION['id']);

                        // $sql = "SELECT * FROM ((courses INNER JOIN studenti ON studenti.course_id = courses.c_id)
                        // INNER JOIN users ON studenti.user_id = users.id) WHERE studenti.user_id = :id";

                        // $sql = "SELECT DISTINCT * FROM courses INNER JOIN studenti ON studenti.course_id = courses.c_id";

                        // $sql = "SELECT DISTINCT * FROM courses INNER JOIN users ON courses.user_id = users.id WHERE courses.user_id = :id";
                        $sql = "SELECT * FROM courses INNER JOIN users ON courses.user_id = users.id WHERE courses.user_id = :id";

                        $result = $pdo->prepare($sql);

                        $result->bindParam(':id', $param_id);

                        $param_id = $id;

                        if ($result->execute()) {
                            if ($result->rowCount() > 0) {
                                while ($row = $result->fetch()) { ?>
                                    <option value="<?= $row['c_id']; ?>"><?= $row['course_name']; ?></option>
                                <?php } ?>
                    </select>
            <?php
                            }
                            unset($result);
                        } else {
                            echo "We dont have record in the DB";
                        }
                        unset($pdo);
            ?>

            <span class="help-block"><?php echo $course_id_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($grade_err)) ? 'has-error' : '' ?>" >
                    <label for="grade">Course Grade</label>

                    <select required id="grade" name="grade">
                        <option disabled selected>Insert grade for the course</option>
                        <?php
                        for ($i = 1; $i <= 5; $i++) { ?>
                            <option value="<?= $i; ?>"> <?= $i; ?> </option>
                        <?php } ?>
                    </select>

                    <!-- <input type="text" name="grade" id="grade" placeholder="Insert grade for the course" value="php echo grade...."> -->
                    <span class="help-block"><?php echo $grade_err; ?></span>
                </div>
                <div class="button-wrapper">
                    <input type="submit" id="yes-btn" class="buttons confirm" value="Submit">
                    <!-- <a href="#" id="yes-btn" class="buttons confirm">Submit</a> -->
                    <a href="index.php" class="buttons cancel">Cancel<i class="fas fa-undo"></i></a>
                </div>
            </form>



        </div>

    </div>
    <?php require "footer.php"; ?>
</body>

</html>