<?php
$title = 'Delete Record - Student Grader';
$page = 'students';
require "navbar.php";


if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}

// if there is zero courses then redirect to create course.php
if (($_SESSION['noCoursesCreated']) == true && isset($_SESSION['noCoursesCreated'])) {
    header('location: create-course.php');
    exit;
}

if (isset($_POST["id"]) && !empty($_POST["id"])) {
    require_once "config.php";

    $sql = "DELETE FROM studenti WHERE studenti.s_id = :id";
    if ($stmt = $pdo->prepare($sql)) {

        $stmt->bindParam(":id", $param_id);

        $param_id = trim($_POST["id"]);

        if ($stmt->execute()) {
            // uspeshno e izbrishano
            header("location: index.php");
        } else {
            echo "smth went wrong";
        }
    }
    unset($stmt);
    unset($pdo);
} else {
    if (empty(trim($_GET["id"]))) {
        header("location: error.php");
        exit();
    }
}
?>

<body>
    <div class="table-container">
        <div class="student-details">
            <h2>Delete Record</h2>
        </div>
        <hr>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="delete-student-container">
            <div class="delete-student">
                <input type="hidden" name="id" value="<?php echo trim($_GET["id"]) ?> " />
                <h5>Are you sure you want to delete the student<b>

                        <?php
                        require_once "config.php";
                        $sql = "SELECT * FROM studenti WHERE s_id = :id";

                        if ($stmt = $pdo->prepare($sql)) {

                            $stmt->bindParam(":id", $param_id);
                            $param_id = trim($_GET["id"]);

                            if ($stmt->execute()) {
                                if ($stmt->rowCount() == 1) {
                                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                                    $name = $row["firstName"].' '.$row['lastName'];
                                } else {
                                    header("location: error.php");
                                    exit();
                                }
                            } else {
                                echo "Smth went wrong";
                            }
                        }
                        unset($stmt);
                        unset($pdo);

                        echo $name;
                        ?> </b>
                    ?</h5>

                <input type="submit" id="yes-btn" class="buttons danger" value="Yes" />
                <!-- <a href="#" id="yes-btn" class="buttons danger">Yes</a> -->
                <a href="index.php" class="buttons confirm">No</a>
            </div>
        </form>
    </div>
    <?php require "footer.php"; ?>
</body>

</html>