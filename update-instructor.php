<?php
$title = 'Update Instructor - Student Grader';
$page = 'edit';
require "navbar.php";

if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}

// if there is zero courses then redirect to create course.php
if (($_SESSION['noCoursesCreated']) == true && isset($_SESSION['noCoursesCreated'])) {
    header('location: create-course.php');
    exit;
}

// include config
require_once "config.php";

// definirame promenlivite od formata i gi inicijalizira so prazna vrednost
$firstName = $lastName = $email = "";
$firstName_err = $lastName_err = $email_err = "";

/*

ako imame POST request isset POST !empty {

    procesiraj go i updajtiraj vo baza
} else {

ako postoi GET["id"], go zemamo toj id od url-to so get i pravime select statement

SELECT * FROM studenti WHERE id = :id;

name
course
grade

}
*/

// if 1 start
if (isset($_POST["id"]) && !empty($_POST["id"])) {
    //ovde kje se izvrshuva update

    //Validacija na name

    $id = $_POST["id"];
    $input_firstName = trim($_POST["firstName"]);
    if (empty($input_firstName)) {
        $firstName_err = "Please enter name";
    } else {
        $firstName = $input_firstName;
    }

    $input_lastName = trim($_POST["lastName"]);
    if (empty($input_lastName)) {
        $lastName_err = "Please enter name";
    } else {
        $lastName = $input_lastName;
    }

    //Validacija na email

    if (empty(trim($_POST["email"]))) {
        $email_err = "Please enter email";
    } elseif (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $email_err= "Invalid email format";
    } else  {
        $email = trim($_POST["email"]);
    }
    if(empty($firstName_err) && empty($lastName_err) && empty($email_err)) {
        $sql = "UPDATE users SET firstName = :firstName, lastName = :lastName, email = :email WHERE id = :id";

        if($stmt = $pdo -> prepare($sql)) {
            $stmt -> bindParam(":firstName", $param_firstName);
            $stmt -> bindParam(":lastName", $param_lastName);
            $stmt -> bindParam(":email", $param_email);
            $stmt -> bindParam(":id", $param_id);

            //set na parametrite
            $param_firstName = $firstName;
            $param_lastName = $lastName;
            $param_email = $email;
            $param_id = $id;

            if($stmt->execute()) {
                header("location: edit-profile.php");
                exit();
            } else {
                echo "Smth went wrong";
            }
            unset($stmt);
        }
        unset($stmt);
        
    }
    unset($pdo);
} else {

    if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
        //ovde go zemame URL parametarot
        $id = trim($_GET["id"]);

        // SQL stmt
        $sql = "SELECT * FROM users WHERE id = :id";

        //Prepare
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":id", $param_id);
            //Set
            $param_id = $id;

            //Execute
            if ($stmt->execute()) {
                if ($stmt->rowCount() == 1) {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);

                    $firstName = $row["firstName"];
                    $lastName = $row["lastName"];
                    $email = $row["email"];
                } else {
                    // nema validen id paramatar i ne nosi na error page-ot
                    header("location: error.php");
                    exit();
                }
            } else {
                echo "SMth went wrong";
            }
        }
        unset($stmt);
        // unset($pdo);
    } else {
        header("location: error.php");
        exit();
    }
}

// if 1 finish

?>

<body>
    <div class="form-container">
        <div class="header-label">
            <h2>Update Instructor Record</h2>
        </div>
        <hr>
        <p>Please edit the input values and submit to update the record.</p>
        <div class="form-update-wrapper">
            <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="form-update">

                <div class="form-group <?php echo (!empty($firstName_err)) ? 'has-error' : ''; ?>">
                    <label for="firstName">First Name</label>
                    <input type="text" name="firstName" id="name" placeholder="Insert first name" value="<?php echo $firstName; ?>">
                    <span class="help-block"><?php echo $firstName_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                    <label for="lastName">Last Name</label>
                    <input type="text" name="lastName" id="name" placeholder="Insert last name" value="<?php echo $lastName; ?>">
                    <span class="help-block"><?php echo $lastName_err; ?></span>
                </div>

                <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                    <label for="email">E-mail</label>
                    <input type="text" name="email" id="name" placeholder="Insert Updated Email" class="mg-btm" value="<?php echo $email; ?>">
                    <span class="help-block"><?php echo $email_err; ?></span>
                </div>

                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <div class="button-wrapper">
                    <input type="submit" id="yes-btn" class="buttons confirm" value="Submit">
                    <!-- <a href="#" id="yes-btn" class="buttons confirm">Submit</a> -->
                    <a href="edit-profile.php" class="buttons cancel">Cancel<i class="fas fa-undo"></i></a>
                </div>
            </form>


        </div>

    </div>
    <?php require "footer.php"; ?>
</body>

</html>