<?php
$title = 'View Record - Student Grader';
$page = 'course';
require "navbar.php";

if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}

if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
    require_once "config.php";

    // prepare SQL
    // $sql = "SELECT * FROM studenti JOIN courses ON studenti.course_id = courses.id";
    // $sql = "SELECT * FROM studenti WHERE id = :id";
    $sql = "SELECT * FROM courses INNER JOIN users ON courses.user_id =  users.id WHERE courses.c_id = :id";

    if ($stmt = $pdo->prepare($sql)) {

        $stmt->bindParam(":id", $param_id);
        $param_id = trim($_GET["id"]);

        if ($stmt->execute()) {
            if ($stmt->rowCount() == 1) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $course_name = $row["course_name"];
                $course_desc = $row["course_desc"];
                $instructor  = $row["firstName"];
                $instructor .= " ".$row["lastName"];
            } else {
                header("location: error.php");
                exit();
            }
        } else {
            echo "Smth went wrong";
        }
    }
    unset($stmt);
    unset($pdo);
} else {
    // url-to
    header("location:error.php");
    exit();
}

?>

<body>
    <div class="form-container">
        <div class="header-label">
            <h2>View Course</h2>
        </div>
        <hr>
        <p>Record from the database for <?= $course_name ?> course.</p>
        <div class="form-update-wrapper">

            <form action="update-form.php" method="POST" class="form-update">
                <label class="view-bold" for="name">Course Name</label>
                <p><?php echo $course_name; ?></p>
                <hr>
                <label class="view-bold" for="course">Course Details</label>
                <p><?php echo $course_desc; ?></p>
                <hr>
                <label class="view-bold" for="course">Instructor Name</label>
                <p class="mg-btm"><?php echo $instructor; ?></p>

                <div class="button-wrapper">
                    <a href="manage-courses.php" class="buttons back">Back to Courses</a>
                </div>
            </form>



        </div>

    </div>

    <?php require "footer.php"; ?>
</body>

</html>