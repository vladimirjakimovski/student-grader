<?php

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true ) {
    header('location: index.php');
    exit;
}



//povikuvanje na config.php za povrzuvanje so baza
require_once "config.php";

//definiranje na promenlivite i nivna inicijalizacija so prazni vrednosti
$firstName = $lastName = $username = $email = $password = $confirm_password = "";
$firstName_err = $lastName_err = $username_err = $email_err = $password_err = $confirm_password_err = "";

//procesiranje na podatocite 

if ($_SERVER['REQUEST_METHOD'] == "POST") {


    //validacija na ime i prezime

    if (empty(trim($_POST["firstname"]))) {
        $firstName_err = "Please enter your name.";
    } else {
        $firstName = trim($_POST["firstname"]);
    }

    if (empty(trim($_POST["lastname"]))) {
        $lastName_err = "Please enter your last name.";
    } else {
        $lastName = trim($_POST["lastname"]);
    }


    //validacija na username
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter username";
    } else {
        //prepare SELECT
        $sql = "SELECT id FROM users WHERE username = :username";

        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":username", $param_username);

            //setiranje na parametri
            $param_username = trim($_POST["username"]);
        }
        //da izvrsime prepared statement
        if ($stmt->execute()) {
            if ($stmt->rowCount() == 1) {
                $username_err = "This username is taken.";
            } else {
                $username = trim($_POST["username"]);
            }
        } else {
            echo "Smth is wrong";
        }
        unset($stmt);
    }
    //Validacija na email
    if (empty(trim($_POST["email"]))) {
        $email_err = "Please enter email";
    } elseif (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $email_err= "Invalid email format";
    } else  {
        $email = trim($_POST["email"]);
    }

    // if (empty(trim($_POST['email']))) {
    //     $email_err = 'Please enter email';
    // } elseif (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == false) {  
    //     $email_err = 'Your email addres is not valid';
    // } else {
    //     $email = trim($_POST['email']);
    // }

     //validacija na password
  if (empty(trim($_POST["password"]))) {
    $password_err = "Please enter a password.";
  } elseif (strlen(trim($_POST["password"])) < 6) {
    $password_err = "Password must have atleast 6 characters.";
  } else {
    $password = trim($_POST["password"]);
  }

  // validacija na confirm password  
  if (empty(trim($_POST["confirm_password"]))) {
    $confirm_password_err = "Please confirm password.";
  } else {
    $confirm_password = trim($_POST["confirm_password"]);
    if (empty($password_err) && ($password != $confirm_password)) {
      $confirm_password_err = "Password did not match.";
    }
  }

    // proverka na input errors

    if (empty($firstName_err) && empty($lastName_err) && empty($username_err) && empty($email_err) && empty($password_err) && empty($confirm_password_err)) {

        //podgotovka za insert stat
        $sql = "INSERT INTO users (username,password,email, firstName, lastName) VALUES (:username, :password, :email, :firstname, :lastname)";

        if ($stmt = $pdo->prepare($sql)) {

            //bind na varijabli
            $stmt->bindParam("firstname", $param_firstname);
            $stmt->bindParam("lastname", $param_lastname);
            $stmt->bindParam("username", $param_username);
            $stmt->bindParam("email", $param_email);
            $stmt->bindParam("password", $param_password);


            //setiranje na parametri 
            $param_firstname = $firstName;
            $param_lastname = $lastName;
            $param_username = $username;
            $param_email = $email;
            $param_password = password_hash($password, PASSWORD_DEFAULT);

            //izvrsuvanje na stmt

            if ($stmt->execute()) {
                //Redirect kon login page
                header("location: login.php");
            } else {
                echo "Smth went wrong.";
            }
            unset($stmt);
        }
    }
    unset($pdo);
} //end na if

$title="Register - Student Grader";
require "navbar.php";
?>


<body>
    <div class="form-container">
        <div class="row">

            <div class="form-update-wrapper">
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">

                    <div class="form-group <?php echo (!empty($firstName_err)) ? 'has-error' : ''; ?>">
                        <label>First name</label>
                        <input type="text" name="firstname" class="form-control" value="<?php echo $firstName; ?>">
                        <span class="help-block"><?php echo $firstName_err; ?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($lastName_err)) ? 'has-error' : ''; ?>">
                        <label>Last name</label>
                        <input type="text" name="lastname" class="form-control" value="<?php echo $lastName; ?>">
                        <span class="help-block"><?php echo $lastName_err; ?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                        <span class="help-block"><?php echo $username_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                        <label>E-mail</label>
                        <input type="email" name="email" class="form-control" value="<?php echo $email; ?>">
                        <span class="help-block"><?php echo $email_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                        <span class="help-block"><?php echo $password_err; ?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                        <label>Confirm Password</label>
                        <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                        <span class="help-block"><?php echo $confirm_password_err; ?></span>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <input type="reset" class="btn btn-default" value="Reset">
                    </div>
                    <p>Already have an account? <a href="login.php">Login here</a>.</p>
                </form>
            </div>

        </div>
    </div>
    <?php require "footer.php"; ?>
</body>

</html>