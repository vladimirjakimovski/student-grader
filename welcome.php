<?php
session_start();

if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}


echo "User ID ";
echo $_SESSION['id'];
echo " Welcome to logged in page ";
echo $_SESSION['username'];

?>


<!DOCTYPE html>
<html>

<head>
    <title>Welcome</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>

<div class="page-header">
    <h1>Hi, <b><?php echo $_SESSION['username']; ?></b> <br>Welcome to the Student Grader App.</h1>
</div>
<p>
<a href="reset-password.php" class="btn btn-warning">Reset Your Password</a>
<a href="logout.php" class="btn btn-danger">Logout <?php echo $_SESSION['username'];?> </a>
</p>
<?php require "footer.php"; ?>
</body>

</html>
