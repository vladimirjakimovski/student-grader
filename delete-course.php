<?php
$title = 'Delete Course - Student Grader';
$page = 'courses';
require "navbar.php";


if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
    header('location: login.php');
    exit;
}


if (isset($_POST["id"]) && !empty($_POST["id"])) {
    require_once "config.php";

    $sql = "DELETE FROM courses WHERE courses.c_id = :id";
    if ($stmt = $pdo->prepare($sql)) {

        $stmt->bindParam(":id", $param_id);

        $param_id = trim($_POST["id"]);
        try {
            if ($stmt->execute()) {
                // uspeshno e izbrishano
                $_SESSION['message'] = 'SUCCESS';
                header("location: manage-courses.php");
            } else {
                throw new Exception('You cannot delete a courses with active students');
                echo "smth went wrong";
            }
        } catch (Exception $e) {
            $_SESSION['error-message'] = $e -> getMessage();
            header("location:manage-courses.php");
        }
    }
    unset($stmt);
    unset($pdo);
} else {
    if (empty(trim($_GET["id"]))) {
        header("location: error.php");
        exit();
    }
}
?>

<body>
    <div class="table-container">
        <div class="student-details">
            <h2>Delete Course</h2>
        </div>
        <hr>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="delete-student-container">
            <div class="delete-student">
                <input type="hidden" name="id" value="<?php echo trim($_GET["id"]) ?> " />
                <h5>Are you sure you want to delete<b>

                        <?php
                        require_once "config.php";
                        $sql = "SELECT * FROM courses WHERE c_id = :id";

                        if ($stmt = $pdo->prepare($sql)) {

                            $stmt->bindParam(":id", $param_id);
                            $param_id = trim($_GET["id"]);

                            if ($stmt->execute()) {
                                if ($stmt->rowCount() == 1) {
                                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                                    $course_name = $row["course_name"];
                                    $course_desc = $row["course_desc"];
                                } else {
                                    header("location: error.php");
                                    exit();
                                }
                            } else {
                                echo "Smth went wrong";
                            }
                        }
                        unset($stmt);
                        unset($pdo);


                        echo $course_name;
                        ?> </b>
                    course?</h5>

                <input type="submit" id="yes-btn" class="buttons danger" value="Yes" />
                <!-- <a href="#" id="yes-btn" class="buttons danger">Yes</a> -->
                <a href="manage-courses.php" class="buttons confirm">No</a>
            </div>
        </form>
    </div>
    <?php require "footer.php"; ?>
</body>

</html>